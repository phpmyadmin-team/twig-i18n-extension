<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;

use \Twig\Environment;
use \Twig\Loader\FilesystemLoader;
use \PhpMyAdmin\Twig\Extensions\I18nExtension;

final class SimpleTest extends TestCase
{

    public function testImplementation(): void
    {
        $tplDir = __DIR__ . '/templates';
        $loader = new FilesystemLoader($tplDir);

        $twig = new Environment($loader, [
            'cache' => false,
        ]);
        $twig->addExtension(new I18nExtension());

        // Load it
        $template = $twig->load('example.twig');
        $this->assertSame(<<<'TEXT'

            Hey Mélanie, I have 3 trains.


        Hello Mélanie!
        TEXT, $template->render([
            'name' => 'Mélanie',
            'train_count' => 3,
        ]));

        $this->assertSame(<<<'TEXT'

            Hey Mélanie, I have one train.


        Hello Mélanie!
        TEXT, $template->render([
            'name' => 'Mélanie',
            'train_count' => 1,
        ]));
    }
}
